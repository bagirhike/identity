package id.co.hike.gateway.organization.model;


public class Query {
	
	private String id;
	private String name;
	private String fspId;
	private String prodId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	public String getFspId() {
		return fspId;
	}
	public void setFspId(String fspId) {
		this.fspId = fspId;
	}
}
