package id.co.hike.gateway.organization.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

import id.co.hike.gateway.organization.model.Organization;
import id.co.hike.gateway.organization.repo.OrgPersistence;

@Component
public class OrgAddRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Organization.class);

		from("direct:startAddOrgConfig").id("addorgconfig").marshal(jsonDataFormat)
		.bean(OrgPersistence.class, "commit")
		.convertBodyTo(String.class).end();
	}
}