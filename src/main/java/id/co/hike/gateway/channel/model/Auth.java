package id.co.hike.gateway.channel.model;

public class Auth {
	
	private String pin;
	private String password;
	private String secret;
	private String orgId;
	private String fspId;
	
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getFspId() {
		return fspId;
	}
	public void setFspId(String fspId) {
		this.fspId = fspId;
	}
}
