package id.co.hike.gateway.channel.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

import id.co.hike.gateway.channel.model.Query;
import id.co.hike.gateway.channel.repo.ChannelPersistence;

@Component
public class ChannelFindRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Query.class);

		from("direct:startGetChannelConfig").id("getchanconfig").marshal(jsonDataFormat)
		.bean(ChannelPersistence.class, "get").end();
	}
}