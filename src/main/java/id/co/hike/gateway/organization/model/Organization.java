package id.co.hike.gateway.organization.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class Organization {
	
	@Id
	private String id;
	
	private String name;
	private String type;
	private List<FSPAuth> fsp;
	private List<Product> products;
	
	@Transient
	private String message;
	
	@Transient
	private FSPAuth onFSP;
	
	@Transient
	private Product onProduct;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<FSPAuth> getFsp() {
		return fsp;
	}
	public void setFsp(List<FSPAuth> fsp) {
		this.fsp = fsp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	public FSPAuth getOnFSP() {
		return onFSP;
	}
	public void setOnFSP(FSPAuth onFSP) {
		this.onFSP = onFSP;
	}
	public Product getOnProduct() {
		return onProduct;
	}
	public void setOnProduct(Product onProduct) {
		this.onProduct = onProduct;
	}

}
