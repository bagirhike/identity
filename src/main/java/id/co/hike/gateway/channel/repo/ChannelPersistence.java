package id.co.hike.gateway.channel.repo;

import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.hike.gateway.channel.model.Query;
import id.co.hike.gateway.channel.model.Channel;

public class ChannelPersistence {

	@Autowired
	private ChannelRepository repo;

	private ObjectMapper obj;

	private String message;

	public String update(String json) throws JsonParseException, JsonMappingException, IOException {

		obj = new ObjectMapper();

		Channel conf = obj.readValue(json, Channel.class);

		repo.save(conf);

		return json;
	}

	public String add(String json) throws JsonParseException, JsonMappingException, IOException {

		obj = new ObjectMapper();

		Channel conf = obj.readValue(json, Channel.class);
		
		if(!repo.existsById(conf.getId())) {
			repo.save(conf);
		}else {
			json = "{\"message\": \"User already registered\"}";
		}

		return json;
	}

	public String get(String json) throws JsonParseException, JsonMappingException, IOException {

		obj = new ObjectMapper();

		Query q = obj.readValue(json, Query.class);

		if (repo.existsById(q.getId())) {
			Optional<Channel> chan = repo.findById(q.getId());
			Channel c = chan.get();
			message = obj.writeValueAsString(c);
		} else {
			message = "{\"message\": \"Channel by ID " + q.getId() + " Not found\"}";
		}

		return message;
	}

}
