package id.co.hike.gateway.channel.model;

public class Client {
	
	private String header;
	private String buildNum;
	private String type;
	
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getBuildNum() {
		return buildNum;
	}
	public void setBuildNum(String buildNum) {
		this.buildNum = buildNum;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
