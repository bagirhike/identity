package id.co.hike.gateway.organization.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

import id.co.hike.gateway.organization.model.Query;
import id.co.hike.gateway.organization.repo.OrgPersistence;

@Component
public class OrgFindRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Query.class);

		from("direct:startGetOrgConfig").id("getorgconfig").marshal(jsonDataFormat)
		.bean(OrgPersistence.class, "get").end();
	}
}