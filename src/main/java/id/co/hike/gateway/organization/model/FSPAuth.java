package id.co.hike.gateway.organization.model;

import java.util.Map;

public class FSPAuth {
	
	private String id;
	private Map<String,Object> auth;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Map<String, Object> getAuth() {
		return auth;
	}
	public void setAuth(Map<String, Object> auth) {
		this.auth = auth;
	}
	
	

}
