package id.co.hike.gateway.organization.controller;

import java.util.concurrent.ExecutionException;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import id.co.hike.gateway.organization.model.Query;
import id.co.hike.gateway.organization.model.Organization;

@RestController
public class OrgController {

	@Produce(uri = "direct:startAddOrgConfig")
	private ProducerTemplate addOrg;
	
	@Produce(uri = "direct:startGetOrgConfig")
	private ProducerTemplate getOrg;
	
	@Produce(uri = "direct:startGetProductConfig")
	private ProducerTemplate getProduct;
	
	@Produce(uri = "direct:startGetProductsConfig")
	private ProducerTemplate getProducts;

	@RequestMapping(value = "/org/add", method = RequestMethod.POST)
	@ResponseBody
	public String addOrgConfiguration(@RequestBody Organization conf) {

		Object msg = addOrg.requestBody(addOrg.getDefaultEndpoint(), conf);
		return msg.toString();
	}
	
	@RequestMapping(value = "/org/get", method = RequestMethod.GET)
	@ResponseBody
	public String findOrganizationbyID(@RequestParam String id, @RequestParam String fspId, @RequestParam String prodId) throws InterruptedException, ExecutionException {
		
		Query query = new Query();
		query.setId(id);
		query.setFspId(fspId);
		query.setProdId(prodId);

		Object msg = getOrg.requestBody(getOrg.getDefaultEndpoint(), query);
		return msg.toString();
	}
	
	@RequestMapping(value = "/org/getproduct", method = RequestMethod.GET)
	@ResponseBody
	public String findProductbyOrg(@RequestParam String id, @RequestParam String prodId) throws InterruptedException, ExecutionException {
		
		Query query = new Query();
		query.setId(id);
		query.setProdId(prodId);

		Object msg = getProduct.requestBody(getProduct.getDefaultEndpoint(), query);
		return msg.toString();
	}
	
	@RequestMapping(value = "/org/getproducts", method = RequestMethod.GET)
	@ResponseBody
	public String findProductsbyOrg(@RequestParam String id, @RequestParam String prodType) throws InterruptedException, ExecutionException {
		Query query = new Query();
		query.setId(id);
		query.setProdId(prodType);

		Object msg = getProducts.requestBody(getProducts.getDefaultEndpoint(), query);
		return msg.toString();
	}
}
