package id.co.hike.gateway.channel.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import id.co.hike.gateway.channel.model.Channel;

public interface ChannelRepository extends MongoRepository<Channel, String> {

	public List<Channel> findByName(String name);

}
