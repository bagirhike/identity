package id.co.hike.gateway.organization.repo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.hike.gateway.organization.model.Query;
import id.co.hike.gateway.organization.model.FSPAuth;
import id.co.hike.gateway.organization.model.Organization;
import id.co.hike.gateway.organization.model.Product;

public class OrgPersistence {

	@Autowired
	private OrgRepository repo;

	private ObjectMapper obj;

	private String message;

	public String commit(String json) throws JsonParseException, JsonMappingException, IOException {

		obj = new ObjectMapper();

		Organization conf = obj.readValue(json, Organization.class);

		repo.save(conf);

		return json;
	}

	public String get(String json) throws JsonParseException, JsonMappingException, IOException {

		obj = new ObjectMapper();

		Query q = obj.readValue(json, Query.class);

		if (repo.existsById(q.getId())) {
			Optional<Organization> org = repo.findById(q.getId());
			Organization o = org.get();

			boolean isExists = false;
			for (FSPAuth f : o.getFsp()) {
				if (f.getId().equals(q.getFspId())) {
					isExists = true;
					o.setOnFSP(f);
					o.setFsp(null);
					break;
				}
			}

			if (isExists) {
				if (q.getProdId().equals("none"))
					message = obj.writeValueAsString(o);
				else {
					isExists = false;
					for (Product p : o.getProducts()) {
						if (p.getProdId().equals(q.getProdId())) {
							isExists = true;
							o.setOnProduct(p);
							o.setProducts(null);
							break;
						}
					}
					
					if (isExists)
						message = obj.writeValueAsString(o);
					else
						message = "{\"message\": \"Product Not found\"}";
				}

			} else {
				message = "{\"message\": \"FSP not found\"}";
			}

		} else {
			message = "{\"message\": \"Organization Not found\"}";
		}

		return message;
	}
	
	public String getProducts(String json) throws JsonParseException, JsonMappingException, IOException {

		obj = new ObjectMapper();
		List<Product> products = new ArrayList<Product>();

		Query q = obj.readValue(json, Query.class);

		if (repo.existsById(q.getId())) {
	
			Optional<Organization> org = repo.findById(q.getId());
			Organization o = org.get();

			for (Product p : o.getProducts()) {
				
				if (p.getType().equals(q.getProdId())) {
					products.add(p);
				}
			}
			
			message = obj.writeValueAsString(products);

		} else {
			message = "{\"message\": \"Organization Not found\"}";
		}

		return message;
	}
	
	public String getProduct(String json) throws JsonParseException, JsonMappingException, IOException {

		obj = new ObjectMapper();

		Query q = obj.readValue(json, Query.class);

		if (repo.existsById(q.getId())) {
			Optional<Organization> org = repo.findById(q.getId());
			Organization o = org.get();

			boolean isExists = false;
			for (Product p : o.getProducts()) {
				if (p.getProdId().equals(q.getProdId())) {
					isExists = true;
					o.setOnProduct(p);
					o.setProducts(null);
					break;
				}
			}
			
			if (isExists)
				message = obj.writeValueAsString(o.getOnProduct());
			else
				message = "{\"message\": \"Product Not found\"}";

		} else {
			message = "{\"message\": \"Organization Not found\"}";
		}

		return message;
	}

}
