package id.co.hike.gateway.organization.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import id.co.hike.gateway.organization.model.Organization;

public interface OrgRepository extends MongoRepository<Organization, String> {

	public List<Organization> findByName(String name);

}
