package id.co.hike.gateway.channel.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

import id.co.hike.gateway.channel.model.Channel;
import id.co.hike.gateway.channel.repo.ChannelPersistence;

@Component
public class ChannelAddRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Channel.class);

		from("direct:startAddChannelConfig").id("addchanconfig").marshal(jsonDataFormat)
		.bean(ChannelPersistence.class, "add").convertBodyTo(String.class).end();
		
		from("direct:startUpdateChannelConfig").id("upchanconfig").marshal(jsonDataFormat)
		.bean(ChannelPersistence.class, "update").convertBodyTo(String.class).end();
		
	}
}