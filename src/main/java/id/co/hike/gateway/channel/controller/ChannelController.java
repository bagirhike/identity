package id.co.hike.gateway.channel.controller;

import java.util.concurrent.ExecutionException;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import id.co.hike.gateway.channel.model.Query;
import id.co.hike.gateway.channel.model.Channel;

@RestController
public class ChannelController {

	@Produce(uri = "direct:startAddChannelConfig")
	private ProducerTemplate addChannel;
	
	@Produce(uri = "direct:startUpdateChannelConfig")
	private ProducerTemplate updateChannel;
	
	@Produce(uri = "direct:startGetChannelConfig")
	private ProducerTemplate getChannel;

	@RequestMapping(value = "/channel/add", method = RequestMethod.POST)
	@ResponseBody
	public String addChannelConfiguration(@RequestBody Channel conf) {

		Object msg = addChannel.requestBody(addChannel.getDefaultEndpoint(), conf);
		return msg.toString();
	}
	
	@RequestMapping(value = "/channel/update", method = RequestMethod.POST)
	@ResponseBody
	public String updateChannelConfiguration(@RequestBody Channel conf) {

		Object msg = updateChannel.requestBody(updateChannel.getDefaultEndpoint(), conf);
		return msg.toString();
	}
	
	@RequestMapping(value = "/channel/get", method = RequestMethod.GET)
	@ResponseBody
	public String findChannelbyID(@RequestParam String id) throws InterruptedException, ExecutionException {
		
		Query query = new Query();
		query.setId(id);

		Object msg = getChannel.requestBody(getChannel.getDefaultEndpoint(), query);
		return msg.toString();
	}
}
