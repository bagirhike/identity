package id.co.hike.gateway.organization.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

import id.co.hike.gateway.organization.model.Query;
import id.co.hike.gateway.organization.repo.OrgPersistence;

@Component
public class OrgProductFindRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Query.class);

		from("direct:startGetProductConfig").id("getproductconfig").marshal(jsonDataFormat)
		.bean(OrgPersistence.class, "getProduct").end();
		
		from("direct:startGetProductsConfig").id("getproductsconfig").marshal(jsonDataFormat)
		.bean(OrgPersistence.class, "getProducts").end();
	}
}